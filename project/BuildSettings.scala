import org.scalastyle.sbt.ScalastylePlugin.autoImport._
import sbt.Keys._
import sbt.{File, TestFrameworks, Tests, _}
import sbtassembly.AssemblyPlugin.autoImport._
import sbtdocker.DockerKeys._
import sbtdocker._

object BuildSettings {
  private val scalaVersionMajor = "2.11"
  private val scalaVersionMinor = scalaVersionMajor + ".8"

  val version = "0.0.1"
  val sparkVersion = "2.4.0"
  val macWireVersion = "2.3.1"

  val defaultSettings = Seq(
    scalaVersion := scalaVersionMinor,
    //Test settings
    fork in Test := true,
    testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-oF"),

    libraryDependencies ++= Seq(
      "org.slf4j" % "slf4j-simple" % "1.7.7",
      "org.json4s" %% "json4s-native" % "3.6.2",
      // Dependency injection
      "com.typesafe" % "config" % "1.3.3",
      "com.github.pureconfig" %% "pureconfig" % "0.9.2",
      // Dependency injection
      "com.softwaremill.macwire" %% "macros" % macWireVersion % "provided",
      "com.softwaremill.macwire" %% "util" % macWireVersion,
      "com.softwaremill.macwire" %% "proxy" % macWireVersion,
      // Tests
      "org.scalatest" %% "scalatest" % "3.0.5" % "test",
      "org.mockito" % "mockito-all" % "1.10.19" % "test"
    )
  )

  def dockerSettings(baseImage: String = "centos:7") = Seq(
    assemblyJarName in assembly := s"jobs-${name.value}.jar",
    dockerfile in docker := {
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from(baseImage)
        add(artifact, artifactTargetPath)
        runRaw("yum install -y mesos-1.3.0 java-1.8.0-openjdk java-1.8.0-openjdk-devel https://centos7.iuscommunity.org/ius-release.rpm")
        runRaw(s"mkdir -p /app/assembly/target/scala-2.12/jars && ln -s $artifactTargetPath /app/assembly/target/scala-2.12/jars/${artifact.name}")
      }
    },

    imageNames in docker := Seq(
      ImageName(
        namespace = Some("voyage"),
        repository = "voyage-recommendations",
        tag = Some(version)
      )
    )
  )

  //Scalastyle
  lazy val compileScalastyle = taskKey[Unit]("compileScalastyle")
  lazy val scalastyleSettings = if (scala.util.Properties.envOrElse("SCALASTYLE", "TRUE") == "TRUE") {
    Seq(
      compileScalastyle := scalastyle.in(Compile).toTask("").value,
      (compile in Compile) := ((compile in Compile) dependsOn compileScalastyle).value
    )
  } else Seq()
}
