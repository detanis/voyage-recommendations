package net.voyage.parser
import scala.xml.{Elem, XML}

class PageParser(sightParser: SightParser) {
  /**
    * Loads and parses XML files
    *
    * @param filename filename for xml file with wikivoyage pages
    * @return parsed pages. Please note that some pages can contain empty list of `Sights`
    */
  def read(filename: String): Seq[Page] = {
    parse(XML.loadFile(filename))
  }

  private def parse(document: Elem): Seq[Page] = {
    (document \ "page").map { pageXml =>
      Page(
        title = (pageXml \ "title").text,
        sights = sightParser.parse((pageXml \ "revision" \ "text").text)
      )
    }
  }
}
