package net.voyage.parser

case class Sight(
    name: Option[String] = None,
    lat: Option[Double] = None,
    long: Option[Double] = None
)

object Sight {
  def fromMap(map: Map[String, String]): Sight = Sight(
    name = getString(map, "name"),
    lat = getDouble(map, "lat"),
    long = getDouble(map, "long")
  )

  private def getDouble(map: Map[String, String], key: String): Option[Double] = {
    getString(map, key).map(_.toDouble)
  }

  private def getString(map: Map[String, String], key: String): Option[String] = {
    map.get(key).filter(_ != "")
  }
}
