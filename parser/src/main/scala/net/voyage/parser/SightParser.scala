package net.voyage.parser

class SightParser() {
  private val sectionPattern = """(?s)(See|Do)==(.+?)(\=\=|$)""".r
  private val sightPattern = """(?s)\{\{(see|do)(.+?)\}\}""".r

  /**
    * 1. Finds in text sections with `* {{ see` <params> `}}` code.
    * 2. Convert found `params` to `Sights` objects.
    * 3. Makes search only in section listed in `sections` parameter.
    *
    * @param text plain text in wikivoyage markup format
    * @return list of sights. Note empty list can be returned if now sights was found
    */
  def parse(text: String): List[Sight] = {
    val sights = for(sectionPattern(_, content, _) <- sectionPattern findAllIn text)
      yield parseSight(content)
    sights.toList.flatten
  }

  private def parseSight(text: String) = {
    val sights = for(sightPattern(_, content) <- sightPattern findAllIn text)
      yield Sight.fromMap(parseParams(content))

    sights.toList
  }

  private def parseParams(row: String) = {
    row.
      replaceAll("!--(.*?)--", "").
      split(raw"\|").
      map(_.split(raw"\=").toList.map(_.trim)).
      collect { case k :: v :: Nil => (k, v) }.toMap
  }
}
