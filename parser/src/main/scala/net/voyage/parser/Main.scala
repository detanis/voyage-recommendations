package net.voyage.parser

import com.softwaremill.macwire.wire
import com.typesafe.config.ConfigFactory
import org.json4s.DefaultFormats
import org.json4s.native.Serialization.writePretty
import org.slf4j.LoggerFactory
import pureconfig.loadConfigOrThrow

import scala.tools.nsc.io.File
class App(
    config: Config,
    pageParser: PageParser
) {
  private val logger = LoggerFactory.getLogger(this.getClass)
  private implicit val formats: DefaultFormats = DefaultFormats

  def process(): Unit = {
    logger.info(s"Reading voyage data from file: ${config.input}")
    val pages = pageParser.read(config.input)
    log(pages)
    writeOutput(pages)
  }

  private def writeOutput(pages: Seq[Page]): Unit = {
    File(config.output).
      writeAll(writePretty(pages.filter(_.sights.nonEmpty)))
  }

  private def log(pages: Seq[Page]): Unit = {
    logger.info(
      s"""Number of pages: ${pages.size}
         |Number of pages with some sights: ${pages.count(_.sights.nonEmpty)}
         |Number of Sights: ${pages.map(_.sights.size).sum}
         |Output was written to: ${config.output}"
       """.stripMargin)
  }
}

object Main {
  def main(args: Array[String]): Unit = (new Module).app.process()
}

case class Config(
    input: String,
    output: String
)

class Module {
  lazy val config = loadConfigOrThrow[Config](ConfigFactory.load().resolve())
  lazy val sightParser = new SightParser()
  lazy val pageParser = wire[PageParser]
  lazy val app = wire[App]
}
