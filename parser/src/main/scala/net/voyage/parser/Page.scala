package net.voyage.parser

case class Page(
    title: String,
    sights: List[Sight]
)
