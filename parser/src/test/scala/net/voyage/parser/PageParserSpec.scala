package net.voyage.parser

import org.scalatest.{FunSpec, Matchers}

class PageParserSpec extends FunSpec with Matchers {
  val subject = new PageParser(new SightParser())

  describe("read") {
    it("reads an XML file") {
      val page :: Nil = subject.read("src/test/resources/data.xml")
      page shouldBe Page(
        title = "Cieszyn",
        sights = List(
          Sight(
            name = Some("Cieszyn Castle"),
            lat = Some(49.750499),
            long = Some(18.627291)
          )
        )
      )
    }
  }
}
