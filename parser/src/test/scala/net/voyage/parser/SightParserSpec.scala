package net.voyage.parser

import org.scalatest.{FunSpec, Matchers}

class SightParserSpec extends FunSpec with Matchers {
  val subject = new SightParser()

  describe("parse") {
    it("parses text to sights") {
      subject.parse(text) shouldBe List(
        Sight(
          name = Some("Cieszyn Castle"),
          lat = Some(49.750499),
          long = Some(18.627291)
        ),
        Sight(
          name = Some("Museum of Cieszyn Silesia"),
          lat = Some(49.74792),
          long = Some(18.63394)
        ),
        Sight(
          name = Some("Lousberg")
        )
      )
    }
  }

  private val text =
    """
      |    ==See==
      |    {{mapframe|49.748436|18.633211|zoom=14}}
      |    [[File:Cieszyn 9891.jpg|thumb|Romanesque St Nicholas rotunda church from the 11th century]]
      |    *'''Old Town Square''' (''Rynek'') with bourgeoisie houses and '''Town Hall''' (''Ratusz'')
      |    * {{see
      |    | name=Cieszyn Castle | alt= | url=http://www.zamekcieszyn.pl/ | email=
      |    | address=Zamkowa 3 | lat=49.750499 | long=18.627291 | directions=
      |    | phone= | tollfree= | fax=
      |    | hours=Su-F: 9:00—17:00. Closed on Sa | price=
      |    | content=Remnants: Piast Castle Tower, Gothic St. Mary Magdalene Church, Romanesque St. Nicholas' Chapel (11th century rotunda)
      |    }}
      |    *Jewish cemeteries
      |    * {{see
      |    | name=Museum of Cieszyn Silesia | alt= | url=http://www.muzeumcieszyn.pl/ | email=
      |    | address=ul. Tadeusza Regera 6 | lat=49.74792 | long=18.63394 | directions=
      |    | phone= | tollfree= | fax=
      |    | hours= | price=16 PLN
      |    |lastedit=2017-03-27| content=In the former Larisch palace (''Pałac Laryszów, Muzeum Śląska Cieszyńskiego''). The oldest continuously working museum in Poland, opened in 1802.
      |    }}
      |
      |    ==Do==
      |
      |    '''[http://www.teatr.cieszyn.pl/ Polish Theatre]''', '''[http://www.tdivadlo.cz/ Czech Theatre]''' and International Theatre '''[http://www.borderfestival.eu/ No Border Festival]'''
      |
      |    * {{do
      |    | name=Lousberg | url= | email=
      |    | address= | lat= | long= | directions=
      |    | phone= | tollfree= | fax=
      |    | hours= | price=
      |    | content=A recreational area situated just north of the city centre that offers stunning views of the city below.
      |    }}
    """.stripMargin
}
