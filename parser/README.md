# voyage-recommendations/parser

App extracts:
- pages - from `page` section
- sights - either `see` or `do` object from `page/revision/text` section

from vikivoyage XML document,
where text is marked down by wikivoyage markdown language

## Config

Config location: `parser/src/main/resources/application.conf`
Fields:

- `input` - XML file with wikivoyage data
- `output` - produced data in JSON format with page and sight information

## Test

`sbt parser/test`

## Run

1. Copy XML file with wikivoyage data to `tmp/wikivoyage`
2. Run `sbt`

## Docker Build

`sbt parser/docker`
