package net.voyage.search

import org.scalatest.{FunSpec, Matchers}

class ScorerSpec extends FunSpec with Matchers {
  val responses = List(
    Fixtures.buildResponse("page1", "sight1", List(0)),
    Fixtures.buildResponse("page2", "sight2", List(1)),
    Fixtures.buildResponse("page3", "sight3", List(0,1))
  )

  val subject = new Scorer(0.9)

  describe("sort") {
    it("scores responses by matched word positions in request term") {
      val first :: second :: third :: Nil = subject.sort(responses)
      first.sight.name should contain("sight3")
      second.sight.name should contain("sight1")
      third.sight.name should contain("sight2")
    }
  }
}
