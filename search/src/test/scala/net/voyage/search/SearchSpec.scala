package net.voyage.search

import org.scalatest.{FunSpec, Matchers}

class SearchSpec extends FunSpec with Matchers {
  val pages = List(
    Fixtures.buildPage(
      "page1",
      List("sight1_1", "sight1_2")
    ),
    Fixtures.buildPage(
      "page2",
      List("sight2_1", "sight2_2")
    ),
    Fixtures.buildPage(
      "page3",
      List("sight3_1 sight3_2")
    )
  )

  val subject = new Search(new Index(pages), new Scorer(0.9))

  describe("find") {
    it("finds sights by sight name") {
      val first :: Nil = subject.find("do sight3_2 search")
      first.sight.name should contain("sight3_1 sight3_2")
    }

    it("finds all sights in page by page name") {
      val first :: second :: Nil = subject.find("do page2 search")
      first.sight.name should contain("sight2_1")
      second.sight.name should contain("sight2_2")
    }

    it(
      """extends search in the following order: sight, page,
        | other sights from already found pages""".stripMargin) {
      val fromSight :: fromPage :: fromFoundPage :: Nil = subject.find("page3 sight2_2")
      fromSight.sight.name should contain("sight2_2")
      fromPage.sight.name should contain("sight3_1 sight3_2")
      fromFoundPage.sight.name should contain ("sight2_1")
    }
  }
}
