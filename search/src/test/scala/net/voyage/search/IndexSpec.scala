package net.voyage.search

import org.scalatest.{FunSpec, Matchers}

class IndexSpec extends FunSpec with Matchers {
  val pages = List(
    Fixtures.buildPage(
      "page1_1 same_page",
      List("sight1_1 same_sight")
    ),
    Fixtures.buildPage(
      "page2_1 same_page",
      List("sight2_1 same_sight")
    )
  )

  val subject = new Index(pages)

  describe("findPages") {
    it("finds one page what contain specific term") {
      val actual :: Nil = subject.findPages("page1_1")
      actual.title shouldBe "page1_1 same_page"
    }

    it("finds all pages what contain specific term") {
      val first :: second :: Nil = subject.findPages("same_page")
      first.title shouldBe "page1_1 same_page"
      second.title shouldBe "page2_1 same_page"
    }
  }

  describe("findSights") {
    it("finds one sight what contain specific term") {
      val actual :: Nil = subject.findSights("sight1_1")
      actual.sight.name should contain("sight1_1 same_sight")
    }

    it("finds all sights what contain specific term") {
      val first :: second :: Nil = subject.findSights("same_sight")
      first.page.title shouldBe "page1_1 same_page"
      first.sight.name should contain("sight1_1 same_sight")
      second.page.title shouldBe "page2_1 same_page"
      second.sight.name should contain("sight2_1 same_sight")
    }
  }
}
