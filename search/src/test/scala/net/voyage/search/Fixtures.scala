package net.voyage.search

object Fixtures {
  def buildPage(title: String, sightNames: List[String]): Page = {
    Page(
      title = title,
      sights = sightNames.map(n => Sight(name = Some(n))))
  }

  def buildResponse(pageTitle: String, sightName: String, positions: List[Int]): Response = {
    Response(
      page = Page(title = pageTitle, sights = List.empty),
      sight = Sight(name = Some(sightName)),
      source = ResponseSources.Page,
      positions = positions
    )
  }
}
