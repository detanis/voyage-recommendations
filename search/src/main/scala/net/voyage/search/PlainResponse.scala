package net.voyage.search

case class PlainResponse(
    user_id: String,
    searched_term: String,
    suggestion_1: Option[String],
    suggestion_2: Option[String],
    suggestion_3: Option[String]

)

object PlainResponse {
  def fromResponse(request: Request, responses: List[Response]): PlainResponse = PlainResponse(
    user_id = request.userId,
    searched_term = request.searchedTerm,
    suggestion_1 = extractName(responses, 0),
    suggestion_2 = extractName(responses, 1),
    suggestion_3 = extractName(responses, 2)
  )

  private def extractName(responses: List[Response], index: Int) = {
    responses.lift(index).flatMap(_.sight.name)
  }
}
