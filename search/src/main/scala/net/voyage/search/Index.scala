package net.voyage.search

class Index(pages: List[Page]) {
  private val pageWords = {
    pages.
      flatMap(page =>
        split(page.title).
          map(_ -> page)
      ).groupBy(_._1).mapValues(_.map(_._2))
  }

  private val sightWords = pages.flatMap { page =>
    page.sights.flatMap(sight =>
      sight.name.map(name =>
        split(name).map(_ -> PageWithSight(page, sight))
      )
    )
  }.flatten.
    groupBy(_._1).mapValues(_.map(_._2))

  def findSights(term: String): List[PageWithSight] = {
    sightWords.getOrElse(term, List.empty)
  }

  def findPages(term: String): List[Page] = {
    pageWords.getOrElse(term, List.empty)
  }

  private def split(term: String): List[String] = {
    term.split(" ").toList
  }
}
