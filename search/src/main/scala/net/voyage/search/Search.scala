package net.voyage.search

case class PageWithSight(
    page: Page,
    sight: Sight
)

class Search(index: Index, scorer: Scorer) {
  def find(termStr: String, limit: Int = 3): List[Response] = {
    val terms = termToList(termStr)
    val fromSights = extendWith(List.empty, limit, () => findSights(terms))
    val fromPages = extendWith(fromSights, limit, () => findFromPages(terms))
    extendWith(fromPages, limit, () => extendFromPages(fromPages.map(_.page)))
  }

  private def extendWith(
    responses: List[Response],
    limit: Int,
    func: () => List[Response]
  ): List[Response] = {
    (if (responses.length < limit) {
      val existNames = responses.map(_.sight.name)
      val newResponses = func().
        filter(r => !existNames.contains(r.sight.name))
      val sorted = scorer.sort(squashResponses(newResponses))
      responses ++ sorted
    } else {
      responses
    }).take(limit)
  }

  private def termToList(term: String) = term.split(" ").toList

  private def extendFromPages(
    pages: List[Page],
    source: ResponseSources.ResponseSource = ResponseSources.ExtendPage,
    positions: List[Int] = List.empty
  ) = {
    pages.flatMap { page =>
      page.sights.zipWithIndex.map { case (sight, positionOnPage) =>
        Response(
          sight = sight,
          page = page,
          source = source,
          positionOnPage = positionOnPage,
          positions = positions
        )
      }
    }
  }

  private def findFromPages(terms: List[String]) = {
    terms.
      zipWithIndex.
      flatMap { case (term, position) =>
        extendFromPages(index.findPages(term), ResponseSources.Page, List(position))
      }
  }

  private def findSights(terms: List[String]) = {
    terms.
      zipWithIndex.flatMap { case (term, position) =>
        index.findSights(term).map(pageWithSight =>
          Response(
            sight = pageWithSight.sight,
            page = pageWithSight.page,
            source = ResponseSources.Page,
            positions = List(position)
          )
        )
      }
  }

  private def squashResponses(responses: List[Response]) = {
    responses.groupBy(_.sight).mapValues { values =>
      values.reduce((agg: Response, v: Response) =>
        agg.copy(positions = (agg.positions ++ v.positions).distinct)
      )
    }.values.toList
  }
}
