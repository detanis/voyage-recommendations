package net.voyage.search

case class Response(
    sight: Sight,
    page: Page,
    source: ResponseSources.ResponseSource,
    positions: List[Int] = List.empty,
    weight: Double = 0,
    positionOnPage: Int = 0
)
