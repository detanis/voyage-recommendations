package net.voyage.search

class Scorer(weightCoef: Double = 0.9) {
  def sort(responses: List[Response]): List[Response] = {
    responses.
      map(r => r.copy(weight = calculateWeight(r.positions))).
      sortBy(r => (-r.weight, r.positionOnPage))
  }

  private def calculateWeight(positions: List[Int]) = {
    if (positions.nonEmpty) {
      positions.map(_ + 1).map(weightDistribution).sum
    }
    else {
      0D
    }
  }

  /**
    * Calculates weight of specific word using logarithmic distribution
    * See: https://en.wikipedia.org/wiki/Logarithmic_distribution
    */

  private def weightDistribution(x: Int) = {
    - Math.pow(weightCoef, x)/(Math.log(1 - weightCoef) * x)
  }
}
