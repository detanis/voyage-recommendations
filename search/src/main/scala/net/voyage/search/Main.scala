package net.voyage.search

import java.io.{File => JFile}

import com.softwaremill.macwire.wire
import com.typesafe.config.ConfigFactory
import kantan.csv._
import kantan.csv.ops._
import kantan.csv.generic._
import org.json4s.DefaultFormats
import org.json4s.native.Serialization.{read, writePretty}
import org.slf4j.LoggerFactory
import pureconfig.loadConfigOrThrow

import scala.io.Source
import scala.tools.nsc.io.File

class App(
    config: Config,
    search: Search
) {
  private val logger = LoggerFactory.getLogger(this.getClass)
  private implicit val formats: DefaultFormats = DefaultFormats

  def process(): Unit = {
    val requests = new JFile(config.inputRequests).toURI.toURL.unsafeReadCsv[List, Request](rfc.withHeader)
    val results = requests.map(result =>
      RequestWithResponses(result, search.find(result.searchedTerm, config.limit))
    )
    writeDebug(results)
    writeOutput(results)
  }

  private def writeOutput(requestWithResponses: List[RequestWithResponses]): Unit = {
    val flatten = requestWithResponses.map(r =>
      PlainResponse.fromResponse(r.request, r.responses)
    )
    new JFile(config.outputCsv).
      writeCsv(flatten, rfc.withHeader)
  }

  private def writeDebug(pages: List[RequestWithResponses]): Unit = {
    File(config.outputDebug).
      writeAll(writePretty(pages))
  }
}

case class Request(
    userId: String,
    searchedTerm: String
)

case class RequestWithResponses(
    request: Request,
    responses: List[Response]
)

object Main {
  def main(args: Array[String]): Unit = (new Module).app.process()
}

case class Config(
    inputPages: String,
    inputRequests: String,
    outputCsv: String,
    outputDebug: String,
    weightCoefficient: Double,
    limit: Int
)

class Module {
  private implicit val formats: DefaultFormats = DefaultFormats

  lazy val config = loadConfigOrThrow[Config](ConfigFactory.load().resolve())
  lazy val pages = read[List[Page]](Source.fromFile(config.inputPages).reader())
  lazy val index = wire[Index]
  lazy val scorer = new Scorer(config.weightCoefficient)
  lazy val search = wire[Search]
  lazy val app = wire[App]
}
