package net.voyage.search

case class Sight(
    name: Option[String] = None,
    lat: Option[Double] = None,
    long: Option[Double] = None
)
