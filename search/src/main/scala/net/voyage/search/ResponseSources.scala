package net.voyage.search

object ResponseSources {
  abstract class ResponseSource(name: String) {
    override def toString: String = name
  }

  case object Page extends ResponseSource("page")
  case object Sight extends ResponseSource("sight")
  case object ExtendPage extends ResponseSource("extend_page")
}
