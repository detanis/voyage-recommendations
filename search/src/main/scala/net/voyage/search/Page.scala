package net.voyage.search

case class Page(
    title: String,
    sights: List[Sight]
)
