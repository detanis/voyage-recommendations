import BuildSettings._
import sbt.Keys.libraryDependencies

lazy val parser = project.in(file("parser")).
  settings(defaultSettings ++ scalastyleSettings ++ dockerSettings()).
  enablePlugins(DockerPlugin).
  settings(name := "parser")

lazy val search = project.in(file("search")).
  settings(defaultSettings ++ scalastyleSettings ++ dockerSettings()).
  enablePlugins(DockerPlugin).
  settings(
    name := "search",
    libraryDependencies ++= Seq(
      "com.nrinaudo" %% "kantan.csv" % "0.5.0",
      "com.nrinaudo" %% "kantan.csv-generic" % "0.5.0"
    )
  )
